const http = require('http')
const {v4} = require('uuid')
data= {
    "slideshow": {
      "author": "Yours Truly",
      "date": "date of publication",
      "slides": [
        {
          "title": "Wake up to WonderWidgets!",
          "type": "all"
        },
        {
          "items": [
            "Why <em>WonderWidgets</em> are great",
            "Who <em>buys</em> WonderWidgets"
          ],
          "title": "Overview",
          "type": "all"
        }
      ],
      "title": "Sample Slide Show"
    }
  }
const port=4000;
const server = http.createServer((request,response)=>{
    switch(request.url){
        case "/html":{
            response.writeHead(200,{"Content-Type":"text/html"})
            response.write(`<!DOCTYPE html>
            <html>
              <head>
              </head>
              <body>
                  <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
                  <p> - Martin Fowler</p>
            
              </body>
            </html>`)
            response.end()
            break
        }
        case "/json":{
            response.writeHead(200,{"Content-Type":"application/json"})
            response.write(JSON.stringify(data))
            response.end()
            break
        }
        case "/uuid":{
            response.writeHead(200,{"Content-Type":"application/json"})
            response.write(JSON.stringify(v4()))
            response.end()
            break
        }
        case "/status/"+request.url.split("/")[2]:{
            if( (request.url.split("/")[2]) in http.STATUS_CODES){
            response.writeHead(request.url.split('/')[2])
            response.write("Status code is "+response.statusCode)}
            else{
                response.writeHead(404)
                response.write("Invalid Status Code")
            }
            response.end()
            break
        }
        case "/delay/"+request.url.split("/")[2]:{
            if(Number.isInteger(request.url.split("/")[2]))
            setTimeout(()=>{
                response.writeHead(200)
                response.write("Status code is "+response.statusCode+" after "+request.url.split("/")[2]+" seconds")
                response.end()
            },request.url.split("/")[2]*1000)
            else{
                response.writeHead(404)
                response.write("Give valied seconds")
                response.end()
            }
            break
        }
        default:{
            response.writeHead(404)
            response.write("Error is "+response.statusCode)
            response.end()
            break
        }
    }
})
server.listen(port)
